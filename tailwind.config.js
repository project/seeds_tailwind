/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["**/*.twig", "*.theme", "inc/**/*.inc", "**/*.tailwind"],
  theme: {
    screens: {
      "min-xs": "640px",
      // => @media (min-width: 640px) { ... }
      "min-sm": "768px",
      // => @media (min-width: 768px) { ... }
      "min-md": "992px",
      // => @media (min-width: 992px) { ... }
      "min-lg": "1024px",
      // => @media (min-width: 1024px) { ... }
      "min-xl": "1280px",
      // => @media (min-width: 1280px) { ... }
      "min-xxl": "1536px",
      // => @media (min-width: 1536px) { ... }
      "max-xxl": { max: "1535px" },
      // => @media (max-width: 1535px) { ... }
      "max-xl": { max: "1279px" },
      // => @media (max-width: 1279px) { ... }
      "max-lg": { max: "1023px" },
      // => @media (max-width: 1023px) { ... }
      "max-md": { max: "991px" },
      // => @media (max-width: 991px) { ... }
      "max-sm": { max: "767px" },
      // => @media (max-width: 767px) { ... }
      "max-xs": { max: "639px" },
      // => @media (max-width: 639px) { ... }
    },
    container: {
      center: true,
      padding: {
        DEFAULT: "15px",
      },
    },
    extend: {
      fontSize: {
        "h1-bold": [
          "30px",
          {
            lineHeight: "75.52px",
            fontWeight: "700",
          },
        ],
        "h1-medium": [
          "30px",
          {
            lineHeight: "75.52px",
            fontWeight: "500",
          },
        ],
        "h1-regular": [
          "30px",
          {
            lineHeight: "71.68px",
            fontWeight: "400",
          },
        ],
      },
      colors: {
        black: "#000000",
        white: "#ffffff",
      },
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("tailwindcss-rtl"),
    require("tailwind-scrollbar")({ nocompatible: true }),
    require("tailwindcss-animated"),
    require("./plugin/tailwindcss-rfs"),
  ],
};
