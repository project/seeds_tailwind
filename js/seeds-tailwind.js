/**
 * @file
 * seeds_tailwind behaviors.
 */
(function (Drupal) {
  "use strict";

  Drupal.behaviors.seedsTailwind = {
    attach(context, settings) {
      let position = window.scrollY || document.documentElement.scrollTop;
      window.addEventListener("scroll", () => {
        const scroll = window.scrollY || document.documentElement.scrollTop;
        if (scroll > 50) {
          document.body.classList.add("scrolled");
        } else {
          document.body.classList.remove("scrolled");
        }
        if (scroll > position) {
          document.body.classList.add("scrolldown");
          document.body.classList.remove("scrollup");
        } else {
          document.body.classList.add("scrollup");
          document.body.classList.remove("scrolldown");
        }
        position = scroll;
      });
    },
  };
})(Drupal);
