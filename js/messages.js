/**
 * @file
 * Customization of messages.
 */

((Drupal, once) => {
  /**
   * Adds a close button to the message.
   *
   * @param {object} message
   *   The message object.
   */
  const closeMessage = (message) => {
    const closeBtn = message.querySelector(".close_button_toast");
    if (closeBtn) {
      closeBtn.addEventListener("click", () => {
        message.classList.add("hidden");
      });
    }
  };

  /**
   * Hides the message container after a delay.
   */
  const hideMessageContainer = () => {
    const messageContainer = document.querySelector(".messages-list");
    if (messageContainer) {
      setTimeout(() => {
        messageContainer.classList.add("hidden");
      }, 5000);
    }
  };

  /**
   * Get messages from context.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the close button behavior for messages.
   */
  Drupal.behaviors.messages = {
    attach(context) {
      hideMessageContainer();
      once("messages", '[data-drupal-selector="messages"]', context).forEach(
        closeMessage
      );
    },
  };

  Drupal.seeds_tailwind.closeMessage = closeMessage;
})(Drupal, once);
