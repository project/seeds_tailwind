/**
 * @file
 * seeds_tailwind behaviors.
 */
(function (Drupal) {
  "use strict";

  Drupal.behaviors.seedsTailwindMenu = {
    attach(context, settings) {
      //toggle responsive menu
      const dropdownToggles = document.querySelectorAll(".mobile-menu-button");
      const dropdownMenu = document.querySelector(".navbar-collapse");
      once("bind-click-event", dropdownToggles, context).forEach(
        (el) => {
          el.addEventListener("click", () => {
            dropdownMenu.classList.toggle("hidden");
          });
        }
      );
    },
  };
})(Drupal);
