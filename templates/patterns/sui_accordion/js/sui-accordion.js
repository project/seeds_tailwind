(function ( Drupal,$,once) {
  "use strict";
  Drupal.behaviors.sui_accordion = {
    attach: function (context, settings) {
      $(once("sui_accordion", ".sui-accordione-title-wrapper")).click(function () {
        $(this).siblings(".sui-accordione-content").slideToggle("600");
        var currentState = $(this).attr("data-open");
        if (currentState === "true") {
            $(this).attr("data-open", "false");
        } else {
            $(this).attr("data-open", "true");
        }
        $(".sui-accordione-title-wrapper")
          .not(this)
          .each(function () {
            $(this).siblings(".sui-accordione-content").slideUp("600");
            $(this).attr("data-open","false");
          });
      });
    },
  };
})(Drupal,jQuery,once);