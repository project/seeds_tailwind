import { CountUp } from "./countup.js/dist/countUp.js";
// Count up.
const content = document.querySelector(".sui-statistic");
let countedUp = false;
document.addEventListener("scroll", function () {
  const scrolled = document.scrollingElement.scrollTop;
  const position = content.getBoundingClientRect().top + window.scrollY;
  if (scrolled > position - window.innerHeight + 100 && !countedUp) {
    loadCountUp();
    countedUp = true;
  }
});
const options = {
  startVal: 0,
  decimalPlaces: 0,
  duration: 4,
  useEasing: true,
  useGrouping: true,
  useIndianSeparators: false,
  smartEasingThreshold: 999,
  smartEasingAmount: 333,
  separator: ",",
  decimal: ".",
  prefix: "",
  suffix: "",
  enableScrollSpy: true,
  scrollSpyDelay: 900,
  scrollSpyOnce: true,
  //Uncomment This to Use Arabic Numbers
  //numerals: ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'],
};
function loadCountUp() {
  const elements = document.querySelectorAll(".sui-statistic");
  elements.forEach(function (element) {
    const fieldElements = element.querySelectorAll(".sui-statistic-num");
    fieldElements.forEach(function (el) {
      const value = el.innerHTML.replace(/\,/g, "");
      const countUp = new CountUp(el, value, options);
      countUp.start();
    });
  });
}
