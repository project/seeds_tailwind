module.exports = (ctx) => ({
  map: ctx.options.map,
  plugins: {
    "postcss-import": {},
    "postcss-utilities": {},
    "tailwindcss/nesting": "postcss-nested",
    autoprefixer: {},
    tailwindcss: {},
    rfs: {},
    "postcss-preset-env": {
      stage: 1,
      preserve: false,
      autoprefixer: {
        cascade: false,
        grid: "no-autoplace",
      },
      features: {
        "blank-pseudo-class": false,
        "focus-visible-pseudo-class": false,
        "focus-within-pseudo-class": false,
        "has-pseudo-class": false,
        "image-set-function": false,
        "prefers-color-scheme-query": false,
        "nesting-rules": [
          "auto",
          {
            edition: "2024-02",
          },
        ],
      },
    },
    ...(process.env.NODE_ENV === "production"
      ? { cssnano: { preset: "default" } }
      : {}),
    ...(process.env.CKEDITOR_BUILD === 'true'
      ? { "postcss-nested-import":{}, }
      : {}),
  },
});
