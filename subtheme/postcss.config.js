module.exports = (ctx) => ({
  map: ctx.options.map,
  plugins: {
    "postcss-import": {},
    "postcss-utilities": {},
    "tailwindcss/nesting": "postcss-nested",
    "postcss-mixins": {},
    "postcss-nested": {},

    autoprefixer: {},
    tailwindcss: {},
    rfs: {
      baseValue: 14, // Default: 20 (which is 1.25rem)
      breakpoint: 1280, // Default: 1200
    },
    "postcss-preset-env": {
      stage: 1,
      preserve: false,
      autoprefixer: {
        cascade: false,
        grid: "no-autoplace",
      },
      features: {
        "blank-pseudo-class": false,
        "focus-visible-pseudo-class": false,
        "focus-within-pseudo-class": false,
        "has-pseudo-class": false,
        "image-set-function": false,
        "prefers-color-scheme-query": false,
        "nesting-rules": [
          "auto",
          {
            edition: "2024-02",
          },
        ],
      },
    },
    ...(process.env.NODE_ENV === "production"
      ? { cssnano: { preset: "default" } }
      : {}),
    ...(process.env.CKEDITOR_BUILD === 'true'
      ? { "postcss-nested-import":{}, }
      : {}),
    ...(process.env.DASHBOARD === 'true'
      ? { "postcss-nested-import":{}, }
      : {}),
  },
});
